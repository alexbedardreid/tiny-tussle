﻿using UnityEngine;
using System.Collections;

//contains information regarding powerUps;
public class PowerUpList : MonoBehaviour {

    [SerializeField]
    public pwrUPInfo[] PowerUps;

	// Use this for initialization
	void Awake () {
        PowerUps = new pwrUPInfo[8];
    }

    void Start()
    {


    }
}

[System.Serializable]
public class pwrUPInfo
{
    [SerializeField]
    string powerUpName;
    [SerializeField]
    GameObject powerUpPrefab;
    [SerializeField]
    float spawnLocation;
    [SerializeField]
    Sprite UISprite;

    public void SetName(string name)
    {
        powerUpName = name;

    }

    public string GetName()
    {
        return powerUpName;

    }
    public GameObject GetObject()
    {
        return powerUpPrefab;

    }
    public Sprite GetSprite()
    {
        return UISprite;

    }
    public float GetSpawnPoint()
    {
        return spawnLocation;

    }
}
