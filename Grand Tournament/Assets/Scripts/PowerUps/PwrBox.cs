﻿using UnityEngine;
using System.Collections;
//spawns the boxes that will grant powerups
//despawns them on collision
public class PwrBox : MonoBehaviour {

    [SerializeField]
    int powerUPID;

    bool active;
    float timer;
    float respawnTime;

    [SerializeField]
    int numberOfPowerUps;

    [SerializeField]
    GameObject box;

    Collider boxCol;
	// Use this for initialization
	void Start () {
        active = true;
        powerUPID = Random.Range(1, numberOfPowerUps);
        boxCol = GetComponent<Collider>();
        timer = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (!active)
        {
            timer++;
            if (timer >= respawnTime)
            {
                Respawn();
            }
        }
    }

    void Respawn()
    {
        SwitchBools(true);
        powerUPID = Random.Range(1, numberOfPowerUps);
        timer = 0;
    }
    //check for collisions with other vehicles
    public void OnCollisionEnter(Collision col)
    {
        //if a player collides with it
        if (col.transform.GetComponent<BasicInput>() != null)
        {
            //that player gets a powerUp;
            //despawn the box for a period of time
            SwitchBools(false);
            col.transform.GetComponent<BasicInput>().NewPowerUp(powerUPID);


        }
    }

    void SwitchBools(bool on)
    {
        active = on;
        boxCol.enabled = on;
        box.SetActive(on);
    }
}
