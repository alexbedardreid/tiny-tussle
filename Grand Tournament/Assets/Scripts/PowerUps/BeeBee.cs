﻿using UnityEngine;
using System.Collections;


//green shell from mario
//shoots forward, bounces off of walls
public class BeeBee : PowerUpsBase{

    [SerializeField]
    GameObject beebee;
    new Rigidbody rigidbody;
    float speed;

    private Vector3 prevPoint;
	// Use this for initialization
	public virtual void Start () {
        SpawnDistance = 3;
        maxTime = 2000;
        base.Start();
        rigidbody = beebee.GetComponent<Rigidbody>();
        speed = 1;
    }
	
	// Update is called once per frame
	public virtual void Update () {
        base.Update();
        uniqueUpdate();
	}

    void uniqueUpdate()
    {
        rigidbody.AddForce(transform.forward * speed);
    }

    public void OnCollisionEnter(Collision col)
    {
        //if it hits a player knock them up
        if(col.gameObject.GetComponent<BasicInput>() != null)
        {
            //knock the player into the air
            //destroy the beebee
            Destroy(gameObject);

        }

        //if it hits a wall continue at reflected angle
        foreach (ContactPoint contact in col.contacts) //Find collision point
        {
            //Find the BOUNCE of the object
           Vector3 temp = Quaternion.AngleAxis(180, contact.normal) * transform.forward * -1;
            transform.forward = temp.normalized;
        }
    }
}
