﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using Recycling;

public class LocalPlayerGameController : MonoBehaviour
{
	//bool ready = false;
	public float RayCheckDistance = 0.54f;
	public GameObject PlayerBall;
	public GameObject mainCamera;
	public GameObject GameCanvas;

	[SerializeField]
	MainUI mainUI;
	public Text UILeaderboardLabel
	{
		get { return mainUI.leaderBoardLabel; }
	}

	[Space(10)]
	[SerializeField]
	GameObject PlayerTrail;
	[SerializeField]
	Material TrailMaterial;

	[Space(10)]
	[SerializeField]
	GameObject ExplosionParticle;

	[Space(10)]
	public List<Transform> spawnPoints = new List<Transform>();

	#region Script Instance

	private static LocalPlayerGameController instance = null;

	protected LocalPlayerGameController() { }

	public static LocalPlayerGameController Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		if (LocalPlayerGameController.Instance && !(LocalPlayerGameController.Instance == this))
			this.gameObject.SetActive(false);
		else
			instance = this;
	}
	#endregion

	// Use this for initialization
	void Start()
	{

	}

	public void InitPlayer(BasicInput ball)
	{
		if (PlayerBall)
			return;

		PlayerBall = ball.gameObject;
		SetupUIReferences(ball);

		//mainCamera.transform.position = PlayerBall.transform.position +
		//	new Vector3(0.05f, 1.1336f, -3.675f);


		//ready = true;
	}

	void SetupUIReferences(BasicInput _bi)
	{
		mainUI.GasButton.inputBeh.PlayerInput = _bi;
		mainUI.ReverseButton.inputBeh.PlayerInput = _bi;

		//mainUI.GasButton.button.onClick.AddListener(() => { _bi.OnGasPressed(); });
		//mainUI.ReverseButton.button.onClick.AddListener(() => { _bi.OnReversePressed(); });
		mainUI.UseButton.onClick.AddListener(() => { _bi.OnUsePressed(); });
	}

	public bool SpawnTrail(Transform currentTrans)
	{
		RaycastHit hit;
		if (Physics.Raycast(currentTrans.position, -Vector3.up, out hit, RayCheckDistance))
		{
			GameObject _trail;
			if (!Recycler.TryGrab(typeof(Decal), out _trail))
			{
				_trail = (GameObject)Instantiate(PlayerTrail);
				Decal _dec = _trail.AddComponent<Decal>();
			}

			//Ray ray = Physics.Raycast(transform.position, -Vector3.up, out hit, 100.0F)

			_trail.transform.position = hit.point + (hit.normal * 0.05f);
            _trail.transform.up = hit.normal;
			return true;
		}


		return false;
	}

	public void RecycleTrail(GameObject deadTrail)
	{
		Recycler.Recycle(typeof(Decal), deadTrail);
	}


	public void SpawnExplosion(Vector3 _explosionPoint, int burst = -1)
	{
		GameObject _sparks;
		if (!Recycler.TryGrab(typeof(Sparks), out _sparks))
		{
			_sparks = (GameObject)Instantiate(ExplosionParticle);
			Sparks _sprk = _sparks.AddComponent<Sparks>();
		}

		if (burst > 0)
			_sparks.GetComponent<Sparks>().Emit(burst);

		_sparks.transform.position = _explosionPoint;
    }

	public void RecycleExplosion(GameObject deadSparks)
	{
		Recycler.Recycle(typeof(Sparks), deadSparks);
	}

	public void VictoryScreen()
	{
		mainUI.GasButton.button.gameObject.SetActive(false);
		mainUI.ReverseButton.button.gameObject.SetActive(false);
		mainUI.UseButton.gameObject.SetActive(false);
	}


#if UNITY_EDITOR

	[ContextMenu("Find All Spawn Points")]
	public void GetSpawnPoints()
	{
		spawnPoints.Clear();

		GameObject[] _temp = GameObject.FindGameObjectsWithTag("Spawn Point");
		foreach(GameObject _go in _temp)
		{
			spawnPoints.Add(_go.transform);
        }
	}
#endif
}

[System.Serializable]
class MainUI
{
	public uiButton GasButton;
	public uiButton ReverseButton;
	public Button UseButton;

	public Text leaderBoardLabel;


}

[System.Serializable]
class uiButton
{
	public Button button;
	public ButtonBehaviour inputBeh;
}