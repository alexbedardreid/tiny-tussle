﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
//base class for power ups
//handles spawning of any prefabs
//updates repective powerup
//inherit information from the player that picks it up
public class PowerUpsBase : MonoBehaviour {

    float timer;
    protected float maxTime;

    //does the powerUp affect you
    [SerializeField]
    bool selfCast;
    [SerializeField]
    protected float SpawnDistance;

    [SerializeField]
    GameObject Player;
    protected Transform playerTrans;


    // Use this for initialization
    public virtual void Start () {
        timer = 0;
        //if it is used on yourself then there is no start position for the powerup
        if (selfCast)
            SpawnDistance = 0;
        else
        {

            //spawn the object a set distance away in the forward direction
            //distances are negative to spawn behind
            playerTrans = Player.transform;
            Vector3 spawnPos = playerTrans.position + playerTrans.forward * SpawnDistance;

            transform.position = spawnPos;
            transform.rotation = playerTrans.rotation;
        }
    }
	
	// Update is called once per frame
	public virtual void Update () {
        if(timer >= maxTime)
        {
            //despawn the powerUp 
            Destroy(gameObject);
            
        }
        timer++;
	}

}
