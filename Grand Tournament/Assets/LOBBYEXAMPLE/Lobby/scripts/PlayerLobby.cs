﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

using System.Collections;
using UnityEngine.EventSystems;

public class PlayerLobby : NetworkLobbyPlayer
{
	//public Canvas playerCanvasPrefab;
	//public Canvas playerCanvas;

	[SyncVar]
	public string playerName = "Player";

	NewPlayerLobby newPlayerLobby;

	public Color thisColor
	{
		get { return cc.myColor; }
	}
	// cached components
	ColorControl cc;
	NetworkLobbyPlayer lobbyPlayer;

	Text readyLabel;
	bool isReady = false;

	void Awake()
	{
		cc = GetComponent<ColorControl>();
		lobbyPlayer = GetComponent<NetworkLobbyPlayer>();

		
	}

	public override void OnClientEnterLobby()
	{

		//if (playerCanvas == null)
		//{
		//	playerCanvas = (Canvas)Instantiate(playerCanvasPrefab, Vector3.zero, Quaternion.identity);
		//	playerCanvas.sortingOrder = 1;
		//}

		if(newPlayerLobby == null)
			newPlayerLobby = NewPlayerLobby.Instance;

		Debug.Log("11Starting "+(isLocalPlayer? "Local":"Client") +" Player..." + 
			(isLocalPlayer ? GuiLobbyManager.s_Singleton.localPlayerName : lobbyPlayer.slot.ToString()));


		readyLabel = newPlayerLobby.AddPlayer(playerName, true, lobbyPlayer);

		if (isReady)
			OnGUIReady();

		readyLabel.text = SetReadyString(isReady);

		if (isLocalPlayer)
			OnStartLocalPlayer();

		//PlayerCanvasHooks hooks = playerCanvas.GetComponent<PlayerCanvasHooks>();
		//hooks.panelPos.localPosition = new Vector3(GetPlayerPos(lobbyPlayer.slot), 0, 0);
		//hooks.SetColor(cc.myColor);
		//hooks.SetReady(lobbyPlayer.readyToBegin);

		EventSystem.current.SetSelectedGameObject(newPlayerLobby.ReadyButton.gameObject);
	}

	public override void OnClientExitLobby()
	{
		//if (playerCanvas != null)
		//{
		//	Destroy(playerCanvas.gameObject);
		//}

		if(newPlayerLobby)
		{
			newPlayerLobby.RemovePlayer(lobbyPlayer);
        }
	}

	public override void OnClientReady(bool readyState)
	{
		//PlayerCanvasHooks hooks = playerCanvas.GetComponent<PlayerCanvasHooks>();
		//hooks.SetReady(readyState);

		readyLabel.text = SetReadyString(readyState);

		isReady = readyState;
    }

	//float GetPlayerPos(int slot)
	//{
	//	var lobby = NetworkManager.singleton as GuiLobbyManager;
	//	if (lobby == null)
	//	{
	//		// no lobby?
	//		return slot * 200;
	//	}
	//
	//	// this spreads the player canvas panels out across the screen
	//	var screenWidth = playerCanvas.pixelRect.width;
	//	screenWidth -= 200; // border padding
	//	var playerWidth = screenWidth / (lobby.maxPlayers-1);
	//	return -(screenWidth / 2) + slot * playerWidth;
	//}

	public override void OnStartLocalPlayer()
	{
		//if (playerCanvas == null)
		//{
		//	playerCanvas = (Canvas)Instantiate(playerCanvasPrefab, Vector3.zero, Quaternion.identity);
		//	playerCanvas.sortingOrder = 1;
		//}

		if (newPlayerLobby == null)
			newPlayerLobby = NewPlayerLobby.Instance;


		if(isLocalPlayer)
		{
			Debug.Log("Starting Local Player..." + lobbyPlayer.slot + ", Local: " + isLocalPlayer);

			newPlayerLobby.InitButtonsforLocalPlayer(
						() =>
						{
							OnGUIReady();
						},

						() =>
						{
							OnGUIColorChange();
						});
		}
		playerName = GuiLobbyManager.s_Singleton.localPlayerName;

		newPlayerLobby.ChangePlayerName(lobbyPlayer, playerName);
		ClientSetName();

		// setup button hooks
		//PlayerCanvasHooks hooks = playerCanvas.GetComponent<PlayerCanvasHooks>();
		//hooks.panelPos.localPosition = new Vector3(GetPlayerPos(lobbyPlayer.slot), 0, 0);
		//hooks.SetColor(cc.myColor);
		//
		//hooks.OnColorChangeHook = OnGUIColorChange;
		//hooks.OnReadyHook = OnGUIReady;
		//hooks.OnRemoveHook = OnGUIRemove;
		//hooks.SetLocalPlayer();
	}


	void OnDestroy()
	{
		//if (playerCanvas != null)
		//{
		//	Destroy(playerCanvas.gameObject);
		//}

		if (newPlayerLobby)
		{
			newPlayerLobby.RemovePlayer(lobbyPlayer);
		}
	}

	[Command]
	void CmdSetMyName(string name)
	{
		playerName = name;
		newPlayerLobby.ChangePlayerName(lobbyPlayer, playerName);
	}

	public void ClientSetName()
	{
		CmdSetMyName(playerName);

	}

	public void SetColor(Color color)
	{
		//var hooks = playerCanvas.GetComponent<PlayerCanvasHooks>();
		//hooks.SetColor(color);

		//Debug.Log("Color Set to " + (color == Color.blue ? "blue" : "Red"));

		readyLabel = NewPlayerLobby.Instance.MovePlayer(
			playerName,
			(color == Color.blue),
			lobbyPlayer);

		

		readyLabel.text = SetReadyString(isReady);
		ClientSetName();
	}

	public void SetReady(bool ready)
	{
		//var hooks = playerCanvas.GetComponent<PlayerCanvasHooks>();
		//hooks.SetReady(ready);

		readyLabel.text = SetReadyString(ready);
    }

	[Command]
	public void CmdExitToLobby()
	{
		var lobby = NetworkManager.singleton as GuiLobbyManager;
		if (lobby != null)
		{
			lobby.ServerReturnToLobby();
		}
	}

	// events from UI system

	void OnGUIColorChange()
	{
		if (isLocalPlayer)
		{
			cc.ClientChangeColor();
			
		}
	}

	void OnGUIReady()
	{
		if (isLocalPlayer)
		{
			if (!isReady)
				lobbyPlayer.SendReadyToBeginMessage();
			else
				lobbyPlayer.SendNotReadyToBeginMessage();
		}
	}

	void OnGUIRemove()
	{
		if (isLocalPlayer)
		{
			ClientScene.RemovePlayer(lobbyPlayer.playerControllerId);

			var lobby = NetworkManager.singleton as GuiLobbyManager;
			if (lobby != null)
			{
				lobby.SetFocusToAddPlayerButton();
			}
		}
	}


	public string SetReadyString(bool ready)
	{
		//if (ready)
		//{
		//	return "Ready";
		//}
		//else
		//{
		//	if (isLocalPlayer)
		//	{
		//		return "Play";
		//	}
		//	else
		//	{
		//		return "Not Ready";
		//	}
		//}

		return (ready ? "Ready" : "Not Ready");
	}
}

