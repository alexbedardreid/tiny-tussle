﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

[System.Serializable]
public enum eMovementDirection
{
    Forward, Reverse
}
public class ButtonBehaviour : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public BasicInput PlayerInput;

	[SerializeField]
	public eMovementDirection Direction;
    bool _pressed = false;

    public void OnPointerDown(PointerEventData eventData)
    {
        _pressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _pressed = false;
    }

    void Start()
    {
		if (!PlayerInput) 
			Debug.LogWarning("No Basic Input Attached", this.gameObject);

    }

    void Update()
    {
		if (!PlayerInput) return;

		if (!_pressed) return;

        switch (Direction)
        {
            case eMovementDirection.Forward:
				PlayerInput.OnGasPressed();
                break;
            case eMovementDirection.Reverse:
				PlayerInput.OnReversePressed();
                break;
            
                // (etc...)
			default: throw new NotImplementedException(Direction + " not implemented.");
        }
    }
}