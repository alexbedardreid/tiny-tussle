﻿using UnityEngine;
using System.Collections;

public class SpinningBox : MonoBehaviour {

    [SerializeField]
    Vector3[] positions;
    int targetID;

    [SerializeField]
    float speed;

    float radius;
    // Use this for initialization
    void Start () {

        positions = new Vector3[2];
        positions[0] = new Vector3(0, 5, 0); 
        positions[1] = new Vector3(0, -5, 0);

        targetID = 0;
        speed = 0.1f;
        for(int x=0;x<positions.Length;x++)
        {
            positions[x] = positions[x] + transform.position;
        }

        radius = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {

        //rotate the cube
        transform.Rotate(0, speed, 0);
        //bounce the cube up and down
        Vector3.MoveTowards(transform.position, positions[targetID], Time.deltaTime * speed);

        if (transform.position.y - positions[targetID].y < radius)
            targetID++;

        if (targetID >= positions.Length)
            targetID = 0;
	}
}
