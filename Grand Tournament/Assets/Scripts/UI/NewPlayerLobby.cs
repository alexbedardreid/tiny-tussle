﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NewPlayerLobby : MonoBehaviour
{

	#region Script Instance

	private static NewPlayerLobby instance = null;

	protected NewPlayerLobby() { }

	public static NewPlayerLobby Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		if (NewPlayerLobby.Instance && !(NewPlayerLobby.Instance == this))
			this.gameObject.SetActive(false);
		else
			instance = this;

		InitList();
    }
	#endregion
	
	public Button ReadyButton;
	public Button ChangeTeamButton;
	public Button exitLobbyButton;

	Color localPlayerColor = new Color(255f/255f, 245 / 255f, 172 / 255f);

	//TODO Probably should add a dictionary to simplify grabbing the values
	public List<PlayerUIElement> playerElements = new List<PlayerUIElement>();

	public void InitButtonsforLocalPlayer(UnityAction ready, UnityAction changeTeam)
	{
		//ReadyButton.onClick.AddListener()
		ReadyButton.onClick.AddListener(ready);

		ChangeTeamButton.onClick.AddListener(changeTeam);

	}

	public void InitReturnButton(UnityAction exitLobby)
	{
		exitLobbyButton.onClick.AddListener(exitLobby);
	}

	#region Players Team Lists
	// Use this for initialization
	void InitList()
	{
		foreach(PlayerUIElement _pue in playerElements)
		{
			_pue.SetActive(false);
        }
	}

	public Text AddPlayer(string Name, bool blueTeam, NetworkLobbyPlayer player)
	{
        foreach (PlayerUIElement _pue in playerElements)
		{
			if (!blueTeam && _pue.indexPosition < 3)
				continue;

			if(!_pue.occupied)
			{
				_pue.SetActive(true);

				_pue.occupied = true;
				_pue.player = player;

				_pue.name = Name;

				_pue.imageBG.color = (player.isLocalPlayer ? localPlayerColor : Color.white);

				return _pue.playerReadyLabel;
            }
		}

		return null;
	}

	public Text MovePlayer(string Name, bool blueTeam, NetworkLobbyPlayer player)
	{
		foreach (PlayerUIElement _pue in playerElements)
		{
			if(_pue.player == player)
			{
				_pue.imageBG.color = Color.white;

                _pue.player = null;
                _pue.occupied = false;
				_pue.playerReadyLabel.text = "Not Ready";
                _pue.SetActive(false);
				break;
			}
        }

		foreach (PlayerUIElement _pue in playerElements)
		{
			if (!blueTeam && _pue.indexPosition <= 3)
				continue;

			if (!_pue.occupied)
			{
				_pue.SetActive(true);

				_pue.occupied = true;
				_pue.player = player;

				_pue.name = Name;

				_pue.imageBG.color = (player.isLocalPlayer ? localPlayerColor : Color.white);

				//TODO Need to check for clean after moving
				CheckForCleanList(!blueTeam);

                return _pue.playerReadyLabel;
			}
		}

		return null;
	}

	public void ChangePlayerName(NetworkLobbyPlayer player, string Name)
	{
        foreach (PlayerUIElement _pue in playerElements)
		{
			if (_pue.player == player)
			{
				_pue.imageBG.color = (player.isLocalPlayer ? localPlayerColor : Color.white);
				_pue.name = Name;
                break;
			}
		}
	}

	public void RemovePlayer(NetworkLobbyPlayer player)
	{
		foreach (PlayerUIElement _pue in playerElements)
		{
			if (_pue.player == player)
			{
				_pue.imageBG.color = Color.white;
				_pue.player = null;
				_pue.occupied = false;
				_pue.SetActive(false);
				break;
			}
		}
	}

	void CheckForCleanList(bool cleanBlue)
	{
		int emptyIndex = -1;

		for (int i = 0; i < playerElements.Count; i++)
		{
			if (!cleanBlue && i <= 3)
				continue;
			else if (cleanBlue && i > 3)
				continue;

			if(emptyIndex < 0)
			{
				if (!playerElements[i].occupied)
					emptyIndex = i;
				else
					continue;
			}


			if (i > emptyIndex && playerElements[i].occupied)
			{
				MovetoIndex(i, emptyIndex);
				emptyIndex = i;
            }

		}
	}

	void MovetoIndex(int from, int to)
	{
		playerElements[to].SetActive(true);
		playerElements[to].occupied = true;
		playerElements[to].player = playerElements[from].player;
		playerElements[to].name = playerElements[from].Name;
		playerElements[to].imageBG.color = 
			(playerElements[from].player.isLocalPlayer ? localPlayerColor : Color.white);

		playerElements[from].imageBG.color = Color.white;
		playerElements[from].player = null;
		playerElements[from].occupied = false;
		playerElements[from].SetActive(false);

	}
	#endregion

}

[System.Serializable]
public class PlayerUIElement
{
	public string Name = "Player";

	[SerializeField]
	bool isRedTeam = false;
	[System.NonSerialized]
	public bool occupied = false;
	[System.NonSerialized]
	public NetworkLobbyPlayer player;


	public int indexPosition = 0;
	public Text playerNameLabel;
	public Text playerReadyLabel;
	public RawImage imageBG;
	public GameObject PlayerSlotObject;

	public string name
	{
		set
		{
			Name = value;
			playerNameLabel.text = ((indexPosition + 1) - (isRedTeam ? 4 : 0)) + ". " + value;
        }
	}

	public void SetActive(bool state)
	{
		//Debug.Log("Setting ["+ indexPosition + "] to " + state);
		PlayerSlotObject.SetActive(state);
    }
}

