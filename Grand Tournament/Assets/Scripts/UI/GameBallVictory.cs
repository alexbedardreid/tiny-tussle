﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameBallVictory : MonoBehaviour
{
	public GameObject Podium;

	[Space(10)]
	public BallGroup firstPos;
	public BallGroup secondPos;
	public BallGroup thirdPos;

	BallGroup[] _renderers;
	GameObject[] _objects;

	[Space(10)]
	public RandomColorParticle[] cannons;

	[Space(10)]
	public GameObject first;
	public GameObject second;
	public GameObject third;

	[Space(10)]
	[SerializeField]
	Camera VictoryCamera;

	// Use this for initialization
	void Awake()
	{
		Podium.SetActive(false);

		VictoryCamera.gameObject.SetActive(false);
		_renderers = new BallGroup[3]
		{
			firstPos,
			secondPos,
			thirdPos
		};

		_objects = new GameObject[3]
		{
			first,
			second,
			third
		};

		_objects[0].SetActive(false);
		_objects[1].SetActive(false);
		_objects[2].SetActive(false);
	}


	public void ShowVictory(List<BallBase> _victors)
	{
		Camera.main.gameObject.SetActive(false);

		Debug.Log("Recieved: " + _victors.Count);

		Podium.SetActive(true);

        int index = 0;
		foreach(BallBase _bb in _victors)
		{
			if (_bb == null)
				continue;

			_objects[index].SetActive(true);

			SetTeam(index, _bb.isBlueTeam);

			_bb.gameObject.SetActive(false);

			index++;
        }

		VictoryCamera.gameObject.SetActive(true);

		Invoke("FireCannons", 0.5f);

		NetGameManager.Instance.CmdUpdateAllLeaderboards();
    }

	void SetTeam(int _index, bool isBlue)
	{
		_renderers[_index].BallRenderer.material = 
			(isBlue ? GameMenuBall._Hamsters.BallMaterial : GameMenuBall._Gerbils.BallMaterial);

		_renderers[_index].RodentRenderer.material =
			(isBlue ? GameMenuBall._Hamsters.RodentMaterial : GameMenuBall._Gerbils.RodentMaterial);
	}

	void FireCannons()
	{
		for(int i = 0; i<cannons.Length; i++)
		{
			cannons[i].PlayEmit();
        }
	}
}
