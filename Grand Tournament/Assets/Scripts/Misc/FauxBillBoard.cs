﻿using UnityEngine;
using System.Collections;

public class FauxBillBoard : MonoBehaviour
{
	public Transform ball;

	public MeshRenderer renderer;
	new Transform transform;

	Transform mainCam;
	// Use this for initialization
	void Start()
	{
		transform = gameObject.transform;
		mainCam = Camera.main.transform;

		renderer.sortingOrder = -10000;
		renderer.sortingLayerName = "Background";
    }

	// Update is called once per frame
	void LateUpdate()
	{

		transform.up = -mainCam.forward;


		transform.position = ((ball.position - mainCam.position)).normalized + ball.position;
    }
}
