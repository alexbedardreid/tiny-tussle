﻿using UnityEngine;
using UnityEngine.UI;
using Sounds;

public class OfflineControlHooks : MonoBehaviour
{
	public delegate void CanvasHook();

	public CanvasHook OnStartHostHook;
	public CanvasHook OnStartServerHook;
	public CanvasHook OnStartClientHook;
	public CanvasHook OnStartMMHook;

	public InputField nameInput;
	public Text addressInput;
	public Button firstButton;

	public string GetAddress()
	{
		return addressInput.text;
	}

	public string GetName()
	{
		return nameInput.text;
	}

	public void SetName(string _name)
	{
		Debug.Log("Set To: " + _name);
		nameInput.text = _name;
	}


	public void UIStartHost()
	{
		if (OnStartHostHook != null)
			OnStartHostHook.Invoke();

		PlayButtonSound();
	}

	public void UIStartServer()
	{
		if (OnStartServerHook != null)
			OnStartServerHook.Invoke();

		PlayButtonSound();
	}

	public void UIStartClient()
	{
		if (OnStartClientHook != null)
			OnStartClientHook.Invoke();

		PlayButtonSound();
	}

	public void UIStartMM()
	{
		if (OnStartMMHook != null)
			OnStartMMHook.Invoke();

		PlayButtonSound();
    }

	void PlayButtonSound()
	{
		SoundManager.PlayEffect(SOUND.BUTTON);
	}
}
