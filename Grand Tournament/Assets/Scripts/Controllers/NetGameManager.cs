﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class NetGameManager : NetworkBehaviour
{
	#region Script Instance

	private static NetGameManager instance = null;

	protected NetGameManager() { }

	public static NetGameManager Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		if (NetGameManager.Instance && !(NetGameManager.Instance == this))
			this.gameObject.SetActive(false);
		else
			instance = this;
	}
	#endregion


	public delegate void UpdateScoreDelegate();

	[SyncEvent]
	public event UpdateScoreDelegate EventUpdateScore;

	public delegate void GameFinishedDelegate();

	[SyncEvent]
	public event GameFinishedDelegate EventGameFinished;

	public GameBallVictory victoryScreen;

	//public List<BallBase> debug;
	Dictionary<string, BallBase> registeredPlayers;
	string lastBump = "";

	[SerializeField]
	int targetScore = 100;

	bool gameVictory = false;

	// Use this for initialization
	void Start()
	{
		if (NetworkClient.active)
		{
			EventUpdateScore += UpdateLeaderBoard;
			EventGameFinished += ShowVictoryScreen;
        }
    }

	/// <summary>
	/// Registers player so we can access it later, for bumping and such.
	/// </summary>
	/// <param name="player"></param>
	public void RegisterPlayer(BallBase player)
	{
		if (registeredPlayers == null)
			registeredPlayers = new Dictionary<string, BallBase>();

		Debug.Log("Register: " + player.playerBallname);


		if(!registeredPlayers.ContainsKey(player.gameObject.name + player.playerControllerId))
		{
			registeredPlayers.Add(player.gameObject.name + player.playerControllerId, player);
		}

		//debug = new List<BallBase>();
		//debug.AddRange(registeredPlayers.Values);

		UpdateLeaderBoard();
    }

	[Command]
	public void CmdUpdateAllLeaderboards()
	{
		EventUpdateScore();
    }

	[Command]
	public void CmdVictory()
	{
		EventGameFinished();
	}

	void UpdateLeaderBoard()
	{
		if(!LocalPlayerGameController.Instance)
		{
			StartCoroutine(MakeDeWait());
			return;
		}

		string _tempLead = "Top Players:\n";
		//TODO Need to add checking for scores and such

		List<BallBase> newList = registeredPlayers.Values.OrderByDescending(x => x.playerScore).ToList();

		int i = 1;

		foreach(BallBase _bb in newList)
		{
			if (i > 3)
				break;

			if(!gameVictory && _bb.playerScore >= targetScore)
			{
				gameVictory = true;
				CmdVictory();

				//TODO Stop tracking score, show victory screen
				break;
            }

			_tempLead += (i++) + ". " + _bb.playerBallname + "\t\t" + _bb.playerScore + "\n";
        }

		LocalPlayerGameController.Instance.UILeaderboardLabel.text = _tempLead;
    }

	IEnumerator MakeDeWait()
	{
		while(!LocalPlayerGameController.Instance)
		{
			yield return null;
		}
		UpdateLeaderBoard();
    }

	void ShowVictoryScreen()
	{
		LocalPlayerGameController.Instance.VictoryScreen();
		Sounds.SoundManager.Instance.transform.parent = victoryScreen.transform;
		Sounds.SoundManager.Instance.transform.localPosition = Vector3.zero;
        Sounds.SoundManager.Instance.Victory();

		Debug.Log("Checking for: " + Mathf.Min(3, registeredPlayers.Values.Count));

		victoryScreen.ShowVictory(
			registeredPlayers.Values.OrderByDescending(x => x.playerScore).ToList()
			.GetRange(0, Mathf.Min(3, registeredPlayers.Values.Count)));
    }

	#region Bump Related Checks and Logic
	Dictionary<NetworkInstanceId, BallInfo?[]> _queuedBumps = new Dictionary<NetworkInstanceId, BallInfo?[]>();
	public void QueueBump(NetworkInstanceId id, NetworkInstanceId otherId, BallInfo myBall)
	{
		if(_queuedBumps.ContainsKey(id))
		{
			_queuedBumps[id][1] = myBall;
			//Need to send approved bump here

			ServerCheckBump(CompareSpeeds(_queuedBumps[id]));
			_queuedBumps.Remove(id);
		}
		else if(_queuedBumps.ContainsKey(otherId))
		{
			_queuedBumps[otherId][1] = myBall;
			//Need to send approved bump here

			ServerCheckBump(CompareSpeeds(_queuedBumps[otherId]));
			_queuedBumps.Remove(otherId);
		}
		else
		{
			_queuedBumps.Add(id, new BallInfo?[2] { myBall, null }); 
			//Make new Entry
		}
	}

	BallInfo[] CompareSpeeds(BallInfo?[] _ballInfo)
	{
		BallInfo[] _order = new BallInfo[2];

		Debug.LogError(_ballInfo[0].Value.transform.name+ " " + _ballInfo[0].Value.VelocityValue + " " +
			_ballInfo[1].Value.transform.name + " " + _ballInfo[1].Value.VelocityValue);


		//Position 0 is faster than position 1
		if (_ballInfo[0].Value.VelocityValue > _ballInfo[1].Value.VelocityValue)
		{
			_order[0] = _ballInfo[0].Value;
			_order[1] = _ballInfo[1].Value;

			return _order;
		}
		else
		{
			_order[0] = _ballInfo[1].Value;
			_order[1] = _ballInfo[0].Value;

			return _order;
		}
	}

	void ServerCheckBump(BallInfo[] _bumps)
	{
		if (!isServer)
			return;


		string name = _bumps[1].transform.name + _bumps[1].playerid;

		Debug.LogError("Called Bump");

		RpcBumpPlayer(name, _bumps[0].velocityDir);

		if (_bumps[1].holdingFlag)
		{
			Debug.LogError("Take Flag From b2");
			//b1 now has flag
			registeredPlayers[_bumps[1].transform.name + _bumps[1].playerid].hasFlag = false;
			registeredPlayers[_bumps[0].transform.name + _bumps[0].playerid].hasFlag = true;
		}

	}

	#region Old Bump Check

	//Calls the server to check a bump
	[Command]
	public void CmdServerCheckBump(BallInfo b1, BallInfo b2)
	{
		if (!isServer) return;

		if(!string.IsNullOrEmpty(lastBump))
		{
			if (lastBump.Contains(b1.transform.name) &&
				lastBump.Contains(b2.transform.name))
			{
				Debug.LogError("Duplicate");
				lastBump = "";
				return;
			}
			Debug.LogError("Non-Duplicate");
		}

		lastBump = b1.transform.name + b2.transform.name;

		BallInfo ball;
		bool isFirst=  SlowerBall(b1, b2, out ball);


		string name = ball.transform.name;

		Debug.LogError("Called Bump");
		RpcBumpPlayer(name, (isFirst ? b2.velocityDir : b1.velocityDir));

		if(isFirst && b2.holdingFlag)
		{
			Debug.LogError("Take Flag From b2");
			//b1 now has flag
			registeredPlayers[b2.transform.name].hasFlag = false;
			registeredPlayers[b1.transform.name].hasFlag = true;
		}
		else if(!isFirst && b1.holdingFlag)
		{
			Debug.LogError("Take Flag From b1");
			//b2 now has flag
			registeredPlayers[b1.transform.name].hasFlag = false;
			registeredPlayers[b2.transform.name].hasFlag = true;
		}

	}

	//Calls all clients to let them know someone was bumped
	[ClientRpc]
	public void RpcBumpPlayer(string player, Vector3 dir)
	{
		BallBase plyr;

		if (registeredPlayers.TryGetValue(player, out plyr))
		{
			//This is where the bump needs to happen, or Called
			if (plyr.isLocalPlayer)
				Debug.LogError("You've been Bumped!");
			else
				Debug.LogError("Bumping " + player);

			plyr.BumpBall(dir);

		}

		
	}
	
	/// <summary>
	/// Returns true if the first ball was the slower, and false if other wa the slower of the 
	/// two. Out does provide the the ball.
	/// </summary>
	/// <param name="b1"></param>
	/// <param name="b2"></param>
	/// <param name=""></param>
	/// <returns></returns>
	bool SlowerBall(BallInfo b1, BallInfo b2, out BallInfo oBall)
	{
        if (b1.VelocityValue > b2.VelocityValue)
		{
			oBall = b2;
            return false;
		}
		else
		{
			oBall = b1;
			return true;
		}
	}
#endregion

	
	#endregion

	#region Respawn out-of-bounds player

	public void RespawnPlayer(Transform _trans)
	{
		if (!isServer)
			return;

		_trans.position = LocalPlayerGameController.Instance.spawnPoints.PickRandom().position;
	}
#endregion
}

public struct BallInfo
{
	public short playerid;
	public Transform transform;
	public Vector3 velocityDir;
	public float VelocityValue;
	public NetworkInstanceId id;

	public bool holdingFlag;

	public BallInfo(Transform Trans, Vector3 Velocity,bool hasFlag, NetworkInstanceId NetID, short _plID)
	{
		transform = Trans;
		velocityDir = Velocity;
		holdingFlag = hasFlag;
        id = NetID;
		playerid = _plID;

		VelocityValue = velocityDir.sqrMagnitude;

	}

	public static bool operator ==(BallInfo c1, BallInfo c2)
	{
		return (c1.id == c2.id);
	}
	public static bool operator !=(BallInfo c1, BallInfo c2)
	{
		return (c1.id != c2.id);
	}

	public override bool Equals(System.Object obj)
	{
		// If parameter is null return false.
		if (obj == null)
		{
			return false;
		}

		// If parameter cannot be cast to Point return false.
		BallInfo p = (BallInfo)obj;
		if ((System.Object)p == null)
		{
			return false;
		}

		return (id == p.id);
	}

	public override int GetHashCode()
	{
		return (int)id.Value;
	}
}

//struct PendingBump
//{
//	BallInfo b1;
//	BallInfo b2;
//	public string name;
//
//	public PendingBump(BallInfo ball1,BallInfo ball2)
//	{
//		name = ball1.transform.name + ball2.transform.name;
//		b1 = ball1;
//		b2 = ball2;
//	}
//
//}
