﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

using AppID = UnityEngine.Networking.Types.AppID;

public class GuiLobbyManager : NetworkLobbyManager
{
	public LobbyCanvasControl lobbyCanvas;
	public OfflineCanvasControl offlineCanvas;
	public OnlineCanvasControl onlineCanvas;
	public ExitToLobbyCanvasControl exitToLobbyCanvas;
	public ConnectingCanvasControl connectingCanvas;
	public PopupCanvasControl popupCanvas;
	public MatchMakerCanvasControl matchMakerCanvas;
	public JoinMatchCanvasControl joinMatchCanvas;

	public string localPlayerName;
	public string onlineStatus;
	static public GuiLobbyManager s_Singleton;

	string accessKey = null;

	void Start()
	{
		PlayerPrefs.SetString("CloudNetworkingId", "212801");
		s_Singleton = this;

		//If no key is present, must be a new user
		if (!PlayerPrefs.HasKey("EncryptionID"))
		{
			PlayerPrefs.SetString("EncryptionID", GenerateKey());
			PlayerPrefs.Save();
		}


		accessKey = PlayerPrefs.GetString("EncryptionID");
		Debug.Log("Username Initialized: " + accessKey);


		localPlayerName = PlayerPrefs.GetString("PrefferedName", "Player");

		offlineCanvas.Show();
	}

	string GenerateKey()
	{
		Guid g = Guid.NewGuid();
		string GuidString = Convert.ToBase64String(g.ToByteArray());
		GuidString = GuidString.Replace("=", "");
		GuidString = GuidString.Replace("+", "");

		return GuidString;
    }

	void OnLevelWasLoaded()
	{
		if (lobbyCanvas != null) lobbyCanvas.OnLevelWasLoaded();
		if (offlineCanvas != null) offlineCanvas.OnLevelWasLoaded();
		if (onlineCanvas != null) onlineCanvas.OnLevelWasLoaded();
		if (exitToLobbyCanvas != null) exitToLobbyCanvas.OnLevelWasLoaded();
		if (connectingCanvas != null) connectingCanvas.OnLevelWasLoaded();
		if (popupCanvas != null) popupCanvas.OnLevelWasLoaded();
		if (matchMakerCanvas != null) matchMakerCanvas.OnLevelWasLoaded();
		if (joinMatchCanvas != null) joinMatchCanvas.OnLevelWasLoaded();
	}

	public void SetFocusToAddPlayerButton()
	{
		if (lobbyCanvas == null)
			return;

		lobbyCanvas.SetFocusToAddPlayerButton();
	}

	// ----------------- Server callbacks ------------------

	public override void OnLobbyStopHost()
	{
		lobbyCanvas.Hide();
		offlineCanvas.Show();
	}

	public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
	{
		//This hook allows you to apply state data from the lobby-player to the game-player
		//var cc = lobbyPlayer.GetComponent<ColorControl>();
		//var playerX = gamePlayer.GetComponent<Player>();
		//playerX.myColor = cc.myColor;
		PlayerLobby _plL = lobbyPlayer.GetComponent<PlayerLobby>();
		BallBase _bb = gamePlayer.GetComponent<BallBase>();

		string name = _plL.playerName;
		_bb.playerBallname = name;
		_bb.SetTeam((_plL.thisColor == Color.blue));
        //gamePlayer.name = name + "_Ball";

		//Debug.Log("What Am I?", lobbyPlayer);
		//Debug.Log("and I?", gamePlayer);
		return true;
	}

	public override void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
	{
		base.OnLobbyServerPlayerRemoved(conn, playerControllerId);

		Debug.LogError(playerControllerId + " Left");
	}

	// ----------------- Client callbacks ------------------

	public override void OnLobbyClientConnect(NetworkConnection conn)
	{
		connectingCanvas.Hide();
	}

	public override void OnClientError(NetworkConnection conn, int errorCode)
	{
		connectingCanvas.Hide();
		StopHost();

		popupCanvas.Show("Client Error", errorCode.ToString());
	}

	public override void OnLobbyClientDisconnect(NetworkConnection conn)
	{
		lobbyCanvas.Hide();
		offlineCanvas.Show();
	}

	public override void OnLobbyStartClient(NetworkClient client)
	{
		if (matchInfo != null)
		{
			connectingCanvas.Show(matchInfo.address);
		}
		else
		{
			connectingCanvas.Show(networkAddress);
		}
	}

	public override void OnLobbyClientAddPlayerFailed()
	{
		popupCanvas.Show("Error", "No more players allowed.");
	}

	public override void OnLobbyClientEnter()
	{
		lobbyCanvas.Show();
		onlineCanvas.Show(onlineStatus);

		exitToLobbyCanvas.Hide();

	}

	public override void OnLobbyClientExit()
	{
		lobbyCanvas.Hide();
		onlineCanvas.Hide();

		if (Application.loadedLevelName == base.playScene)
		{
			exitToLobbyCanvas.Show();
		}
	}

#if UNITY_ANDROID

	void OnApplicationPause()
	{
		Application.Quit();
	}

	void LateUpdate()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if (Application.loadedLevel == 0)
			{ Application.Quit(); }
		}
	}

#endif
}
