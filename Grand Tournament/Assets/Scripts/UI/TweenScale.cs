﻿using UnityEngine;
using System.Collections;

public class TweenScale : MonoBehaviour
{
	public AnimationCurve curve = new AnimationCurve();
	public Vector2 ToandFrom = Vector2.one;

	public float speed = 0.5f;

	public bool useDelays = true;

	new Transform transform;
	// Use this for initialization
	void OnEnable()
	{
		transform = gameObject.transform;

		if(ToandFrom.x != ToandFrom.y)
		{
			StartCoroutine(ScalePong());
		}
    }

	float lerp = 0f;

    IEnumerator ScalePong()
	{
		lerp = 0f;

		if(useDelays)
			yield return new WaitForSeconds(Random.Range(0f,1f));

		while(true)
		{
			while(lerp < 1f)
			{
				transform.localScale = 
					Vector3.Lerp(transform.localScale, Vector3.one * ToandFrom.x, CurveValue(ref lerp));

				yield return null;
			}

			if(useDelays)
				yield return new WaitForSeconds(Random.Range(0f, 0.3f));

			lerp = 0f;
			while (lerp < 1f)
			{
				transform.localScale =
					Vector3.Lerp(transform.localScale, Vector3.one * ToandFrom.y, CurveValue(ref lerp));

				yield return null;
			}

			lerp = 0f;
        }
	}

	float CurveValue(ref float _lerp)
	{
		_lerp = _lerp + (Time.deltaTime * speed);

		return curve.Evaluate(_lerp);
    }
}
