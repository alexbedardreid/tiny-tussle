﻿using UnityEngine;
using System.Collections;

namespace Sounds
{
	public class SoundManager : MonoBehaviour
	{
		#region Script Instance

		private static SoundManager instance = null;

		protected SoundManager() { }

		public static SoundManager Instance
		{
			get
			{
				return instance;
			}
		}

		void Awake()
		{
			if (SoundManager.Instance && !(SoundManager.Instance == this))
				this.gameObject.SetActive(false);
			else
				instance = this;

			Init();
		}
		#endregion

		public float MasterVolume = 0.6f;

		public AudioClip mainMusic;
		public AudioClip powerupMusic;
		public AudioClip VictoryMusic;

		public AudioSource musicLoop;
		public AudioSource mainSource;

		public AnimationCurve FadeCurve = new AnimationCurve();

		public SoundEffects soundEffects = new SoundEffects();

		bool started = false;
		// Use this for initialization
		void Init()
		{
			mainSource.volume = MasterVolume;
			

			if (!started && gameObject.activeInHierarchy)
				StartCoroutine(FadeInMusic());
		}

		IEnumerator FadeInMusic()
		{
			musicLoop.volume = 0f;
			float lerp = 0f;

			while (lerp < 1f)
			{
				musicLoop.volume = Mathf.Lerp(0f, MasterVolume, FadeCurve.Evaluate(lerp += Time.deltaTime));

				yield return null;
			}

			DontDestroyOnLoad(this.gameObject);
			started = true;
		}

		bool fading = false;
		public void HasSpecial(bool _power)
		{
			if (fading)
				return;

			StartCoroutine(FadeToMusic((!_power ? mainMusic : powerupMusic), 3f));


		}

		public void Victory()
		{
			if (fading)
				return;

			StartCoroutine(FadeToVictory(mainMusic, 3f));
		}

		IEnumerator FadeToMusic(AudioClip _toClip, float speed)
		{
			fading = true;
            musicLoop.volume = 0f;
			float lerp = 0f;

			while (lerp < 1f)
			{
				musicLoop.volume = Mathf.Lerp(MasterVolume, 0f, FadeCurve.Evaluate(lerp += (Time.deltaTime * speed)));

				yield return null;
			}
			musicLoop.Stop();
			yield return null;

			lerp = 0f;
			musicLoop.clip = _toClip;
			musicLoop.Play();

			while (lerp < 1f)
			{
				musicLoop.volume = Mathf.Lerp(0f, MasterVolume, FadeCurve.Evaluate(lerp += (Time.deltaTime * speed)));

				yield return null;
			}

			fading = false;
        }

		IEnumerator FadeToVictory(AudioClip _toClip, float speed)
		{
			fading = true;
			musicLoop.volume = 0f;
			float lerp = 0f;

			while (lerp < 1f)
			{
				musicLoop.volume = Mathf.Lerp(MasterVolume, 0f, FadeCurve.Evaluate(lerp += (Time.deltaTime * speed)));

				yield return null;
			}
			musicLoop.Stop();
			yield return null;

			lerp = 0f;
			musicLoop.clip = _toClip;
			musicLoop.Play();

			musicLoop.PlayOneShot(VictoryMusic);

			while (lerp < 1f)
			{
				musicLoop.volume = Mathf.Lerp(0f, MasterVolume/2f, FadeCurve.Evaluate(lerp += (Time.deltaTime * speed)));

				yield return null;
			}

			fading = false;
		}

		public static void PlayEffect(SOUND effect, bool overide = true, float volume = -1f)
		{
			instance.playButton(effect, overide, volume);
        }

		void playButton(SOUND _effect, bool overide, float _volume)
		{
			AudioClip _clip = soundEffects.GetAudioClip(_effect);

			if (_clip == null)
			{
				Debug.LogError("No clip found for: "  + _effect);
				return;
			}

			if (_volume <= 0f)
				mainSource.volume = MasterVolume;

			if (overide)
			{
				mainSource.PlayOneShot(_clip, 1f);
			}
			else
			{
				if(_volume > 0f)
					mainSource.volume = _volume;

                mainSource.pitch = Random.Range(0.8f, 1.2f);

                mainSource.clip = _clip;
				mainSource.Play();
            }


		}
	}


	[System.Serializable]
	public class SoundEffects
	{
		public AudioClip ButtonPress;
		public AudioClip Bump;
		public AudioClip FallImpact;

		public AudioClip takeFlag;
		public AudioClip dropFlag;

		public AudioClip GetAudioClip(SOUND _effect)
		{
			switch (_effect)
			{
				case SOUND.BUTTON:
					return ButtonPress;
				case SOUND.BUMP:
					return Bump;
				case SOUND.FALL:
					return FallImpact;
				case SOUND.TAKE_FLAG:
					return takeFlag;
				case SOUND.DROP_FLAG:
					return dropFlag;
				default:
					break;
			}

			return null;
		}
	}

	public enum SOUND : int
	{
		BUTTON = 0,
		BUMP,
		FALL,
		TAKE_FLAG,
		DROP_FLAG

	}
}
