﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;

using Sounds;

[NetworkSettings(channel = 1, sendInterval = 0.1f)]
public class BallBase : NetworkBehaviour
{
	public BallGroup thisBall;

	[SyncVar(hook = "FlagPickedUp")]
	public bool hasFlag = false;

	public GameObject target;
	public GameObject Carrot;

	[SyncVar(hook = "SetTeam")]
	public bool isBlueTeam;

	[SyncVar]
	public string playerBallname = "";

	[SyncVar]
	public int playerScore = 0;

	bool teamSet = false;

	float timer = 0f;

	[SyncVar]
	public Vector3 currentVelocity;

	//void setVel(Vector3 _vec)
	//{
	//	currentVelocity = _vec;
    //}

	//[SyncVar]
	public Vector3 networkVelocity
	{
		get
		{
			if (!isLocalPlayer)
			{
				return currentVelocity;
			}
	
			return Vector3.zero;
		}
	}

	public BasicInput basicInput;

	[SyncVar]
	public bool bumped = false;

	NetworkInstanceId thisNetId;

	new Transform transform;

	bool registered = false;

	AudioSource NetworkAudioPlayer;

	[SerializeField]
	ParticleSystem partsSystemDirt;
    float trailTime = 0f;

	#region Velocity

	public Vector3 Velocity
	{
		get { return (currentPosition - lastPosition) / Time.deltaTime; }
	}
	Vector3 lastPosition;
	Vector3 currentPosition;
	#endregion

	// Use this for initialization
	void Start()
	{
		transform = gameObject.transform;
		target.SetActive(false);

        if (!basicInput)
		{
			basicInput = gameObject.GetComponent<BasicInput>();
		}

		thisNetId = netId;

		if (isLocalPlayer)
		{
			currentPosition = transform.position;
			lastPosition = currentPosition;
		}
		else
		{
			NetworkAudioPlayer = basicInput.engineAudio;

            basicInput.enabled = false;

			if (!partsSystemDirt)
				partsSystemDirt = GetComponentInChildren<ParticleSystem>();
        }

		
	}

	public void SetTeam(bool isBlue)
	{
		isBlueTeam = isBlue;

        thisBall.BallRenderer.material = 
			(isBlueTeam ? GameMenuBall._Hamsters.BallMaterial : GameMenuBall._Gerbils.BallMaterial);

		thisBall.RodentRenderer.material =
			(isBlueTeam ? GameMenuBall._Hamsters.RodentMaterial : GameMenuBall._Gerbils.RodentMaterial);

		gameObject.name = playerBallname + "_Ball";

		teamSet = true;
    }

	public void FlagPickedUp(bool _pickedUp)
	{
		Carrot.SetActive(_pickedUp);

		if(!isLocalPlayer)
		{
			target.SetActive(_pickedUp);
		}
		
		if(isLocalPlayer)
		{
			if (_pickedUp)
			{
				SoundManager.PlayEffect(SOUND.TAKE_FLAG);
            }
			else
			{
				SoundManager.PlayEffect(SOUND.DROP_FLAG);
			}

			SoundManager.Instance.HasSpecial(_pickedUp);
        }
	}

	[Command]
	void CmdSetNetVolicty(Vector3 _velocity)
	{
		currentVelocity = _velocity;
	}

	[Command]
	void CmdSetScore(int _score)
	{
		playerScore = _score;
	}

	public float _magn = 0f;
	// Update is called once per frame
	void LateUpdate()
	{
		if (!registered)
		{
			if (NetGameManager.Instance == null)
				return;
			else
			{
				if (!teamSet)
					SetTeam(isBlueTeam);

				NetGameManager.Instance.RegisterPlayer(this);
				registered = true;
			}
		}
		

		if (isLocalPlayer)
		{
			lastPosition = currentPosition;
			currentPosition = transform.position;

			////This needs to be set after lastPosition, and CurrentPosition are set
			currentVelocity = Velocity;

			CmdSetNetVolicty(currentVelocity);

		}
		else if (!isLocalPlayer)
		{
			_magn = currentVelocity.sqrMagnitude;

            NetworkAudioPlayer.volume = Mathf.Lerp(0f, 1f, _magn / 100f);
			NetworkAudioPlayer.pitch = Mathf.Lerp(0.8f, 1.1f, _magn / 100f);

			if (_magn > 1f)
			{
				partsSystemDirt.emissionRate = 10f;

				if (trailTime > 0.025f)
				{
					LocalPlayerGameController.Instance.SpawnTrail(transform);
					trailTime = 0f;
				}
				else
					trailTime += Time.deltaTime;
			}
			else
			{
				partsSystemDirt.emissionRate = 0f;
			}
		}

		if(isServer)
		{
			if (hasFlag && timer >= 1f)
			{
				++playerScore;
				timer = 0f;

				CmdSetScore(playerScore);

				NetGameManager.Instance.CmdUpdateAllLeaderboards();
            }
			else if(hasFlag)
				timer += Time.deltaTime;

			if (!hasFlag && timer > 0f)
				timer = 0f;
        }
	}

	[Command]
	public void CmdSetBumped(bool _bumped)
	{
		bumped = _bumped;
    }

	public void BumpBall(Vector3 direction)
	{
		if (!isLocalPlayer)
			return;
		if (bumped)
			return;

		basicInput.BumpMe(direction, 0.5f);

	}

	#region Collisions
	private void OnCollisionEnter(Collision col)
	{
		//check for collisions with other players for bump mechanic
		if (col.gameObject.tag.Equals("Player"))
		{
			SoundManager.PlayEffect(SOUND.BUMP, false, 1f);
			LocalPlayerGameController.Instance.SpawnExplosion(col.contacts[0].point);

			if (isServer)
				OnBallEnter(col.gameObject.GetComponent<BallBase>());
		}
		else if(col.gameObject.tag.Equals("Respawn") && isServer)
		{
			NetGameManager.Instance.RespawnPlayer(transform);
        }
		else if (col.gameObject.tag.Equals("Flag"))
		{
			LocalPlayerGameController.Instance.SpawnExplosion(transform.position, 30);

			if(isServer)
			{
				this.hasFlag = true;
				NetworkServer.Destroy(col.gameObject);
			}
		}
	}

	//Server checks the collision to see who has won bump
	protected virtual void OnBallEnter(BallBase other)
	{
		if (bumped || other.bumped) return;

		//NetGameManager.Instance.CmdServerCheckBump(
		//	new BallInfo(transform, Velocity, hasFlag, thisNetId),
		//	new BallInfo(other.transform, other.Velocity, other.hasFlag, other.thisNetId));

		NetGameManager.Instance.QueueBump(thisNetId, other.thisNetId,
			new BallInfo(transform, currentVelocity, hasFlag, thisNetId, playerControllerId));
    }

	#endregion
}
