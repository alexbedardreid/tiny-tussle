﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

//handles the use of the powerup
//the type of the powerup you have

public class OnUseButtonBehaviour : MonoBehaviour {

    //sprite to show having no powerup
    [SerializeField]
    Sprite DefualtImage;

    Image currentImage;

    [SerializeField]
    GameObject powerUpList;
    PowerUpList List;

    GameObject powerUp;

    // Use this for initialization
    void Start()
    {
        currentImage = GetComponent<Image>();
        List = powerUpList.GetComponent<PowerUpList>();
    }

    public void OnUse()
    {
        //call the powerUP spawn function
        //reset the ui to show no pwerUP
        currentImage.sprite = DefualtImage;
       
    }

    //when picking up a new powerUp;
    //get the info from the powerUpBox
    //the sprite to display
    //the powerup to spawn on use
    public void NewPowerUp(int ID)
    {

        int powerID = ID;
        //find the powerUpinfo with that info
       currentImage.sprite = List.PowerUps[ID].GetSprite();
        powerUp = List.PowerUps[ID].GetObject();
        //currentImage  = the respective image for that powerup
    }
}

