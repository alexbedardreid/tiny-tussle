﻿using UnityEngine;
using UnityEngine.Networking;

public class ColorControl : NetworkBehaviour
{
	[SyncVar]
	public int ind = 0;
	static Color[] colors = new Color[] 
	{ Color.blue, Color.red};

	[SyncVar(hook="OnMyColor")]
	public Color myColor = Color.blue;

	NetworkLobbyPlayer lobbyPlayer;
	PlayerLobby playerUI;

	void Awake()
	{
		lobbyPlayer = GetComponent<NetworkLobbyPlayer>();
		playerUI = GetComponent<PlayerLobby>();
	}

	[Command]
	void CmdSetMyColor(Color col)
	{
		// cant change color after turning ready
		if (lobbyPlayer.readyToBegin)
		{
			return;
		}

		myColor = col;
	}

	public void ClientChangeColor()
	{
		if (ind == 1)
			ind = 0;
		else
			ind++;

		Color newCol = colors[ind];
		CmdSetMyColor(newCol);

		GameMenuBall.Instance.SetTo((newCol == Color.blue));

	}

	void OnMyColor(Color newColor)
	{
		myColor = newColor;
		playerUI.SetColor(newColor);

		

	}

	void Update()
	{
		if (!isLocalPlayer)
			return;
	}
}
