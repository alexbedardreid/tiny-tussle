﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RandomColorParticle : MonoBehaviour
{
	public int SpawnNumber = 50;

	public Color[] Colors;

	ParticleSystem particleSys;

	System.Random _rand = new System.Random(123456789);

	void Start()
	{
		particleSys = GetComponent<ParticleSystem>();
		
		//PlayEmit();
    }

	[ContextMenu("Emit Particles")]
	public void PlayEmit()
	{
		if (particleSys == null)
			return;

		if (Colors == null || Colors.Length <= 0)
			return;

		for (int i = 0; i < SpawnNumber; i++)
		{

			particleSys.startColor = Colors[_rand.Next(0, Colors.Length)];

			particleSys.Emit(1);
        }
	}
}
