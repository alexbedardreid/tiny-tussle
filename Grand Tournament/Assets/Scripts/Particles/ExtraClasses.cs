﻿using UnityEngine;
using System.Collections;

//Blank Class to help with Type Sorting
public class Decal : MonoBehaviour
{
	float lerp = 0f;
	bool active = false;
	new Transform transform;

	void OnEnable()
	{
		transform = gameObject.transform;

		active = true;
    }

	void LateUpdate()
	{
		if(active)
		{
			//Consider using Vector2 Lerp
			transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, lerp += Time.deltaTime * 2f);

			if(lerp >= 1f)
			{
				RecycleMe();
            }
        }

	}

	void RecycleMe()
	{
		active = false;
		transform.localScale = Vector3.one;
		lerp = 0f;
		LocalPlayerGameController.Instance.RecycleTrail(this.gameObject);
    }
}

public class Sparks : MonoBehaviour
{

	ParticleSystem _parts;

	new Transform transform;

	public void Emit(int amount)
	{
		_parts.Emit(amount);
    }

	void Awake()
	{
		_parts = this.GetComponent<ParticleSystem>();

		transform = gameObject.transform;
    }

	void OnEnable()
	{
		if(!_parts.isPlaying)
			_parts.Play();

		_parts.Emit(15);
    }


	void LateUpdate()
	{
		if (!_parts.IsAlive())
		{
			//Once all the particles are dead we can recycle this
			RecycleMe();
		}

	}

	void RecycleMe()
	{
		_parts.Stop();
        LocalPlayerGameController.Instance.RecycleExplosion(this.gameObject);
	}
}