﻿using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;
using System;
using System.Collections;

public class BasicInput : NetworkBehaviour
{
	public AudioSource engineAudio;

	public float speed = 1f;
	float realSpeed;

	float currentSpeed = 0f;
	bool reverse = false;


	bool bumped = false;
	Vector3 bumpDirection = Vector3.zero;
	float bumpTimer = 0f;

	[SerializeField]
	GameObject player;
	[SerializeField]
	Transform CameraPoint;
	[SerializeField]
	GameObject ballObject;
	[SerializeField]
	BallBase baseBall;

	ThirdPersonCam cam = null;


	[SerializeField]
    GameObject powerUp;

	//new Rigidbody rigidbody;
	new Transform transform;
	Transform ballTrans;

    [SerializeField]
    GameObject OnUseButton;

	[Space(10)]
	[SerializeField]
	ParticleSystem partsSystemDirt;

	float soundDelay = 0f;
	bool playFallImpact = false;
	float trailTime = 0f;
	float moveRate = 0f;

	#region Mono functions
	// Use this for initialization
	void Start()
	{
		if (!player) 
			throw new NotImplementedException("No Player Ball Object Set.");

		transform = player.transform;
		ballTrans = ballObject.transform;


		//Hook up all needed local values if this is the local player
		if (isLocalPlayer)
		{
			LocalPlayerGameController cntrl = LocalPlayerGameController.Instance;
			if (cntrl == null || this == null)
				throw new NullReferenceException("cntrl is null: " + (cntrl == null) + ", Basic input null: " + (cntrl == null));
			cntrl.InitPlayer(this);

			cam = cntrl.mainCamera.GetComponent<ThirdPersonCam>();
			cam.playerTrans = transform;
			cam.playerCameraPointTrans = CameraPoint;
			cam.Init();

			Transform _sm = Sounds.SoundManager.Instance.transform;
			_sm.parent = cam.transform;
			_sm.localPosition = Vector3.zero;

			engineAudio.spatialBlend = 0f;

			realSpeed = speed;

		}

	}

	float rot = 0f;
	Vector3 baseEuler = Vector3.zero;
	Vector3 eulerConvert = Vector3.zero;

	// Update is called once per frame
	void LateUpdate()
	{
		if (!isLocalPlayer)
			return;

		moveRate = currentSpeed / speed;

		realSpeed = (baseBall.hasFlag ? speed * 0.9f : speed);

		//rotate ball with tilt
		Vector3 Dir = Vector3.zero;

#if UNITY_EDITOR || UNITY_STANDALONE
        Dir.y = Input.GetAxis("Horizontal") * (reverse ? -1.2f: 1.2f);

		if (Input.GetKey(KeyCode.W) && !bumped)
        {
            OnGasPressed();
        }
		else if (Input.GetKey(KeyCode.S) && !bumped)
        {
            OnReversePressed();
        }

#elif UNITY_ANDROID
        Dir.y =  CrossPlatformInputManager.GetAxis("Horizontal") * (reverse ? -2f: 2f);
#endif
		//Prevents the Phantom Spinning that was occuring
		if (Mathf.Abs(Dir.y) > 0.05f && !bumped)
		{
			transform.Rotate(Dir, Space.World); 
		}
		
		///////////////////////////////////////////////////////////////
		/////This Rotates forward or backwards to simulate motion//////
		///////////////////////////////////////////////////////////////
		ballTrans.eulerAngles = transform.eulerAngles;

		eulerConvert.x += (currentSpeed * (reverse ? 5f : -5f)) / realSpeed;

		ballTrans.Rotate(eulerConvert, Space.Self);

		baseEuler = transform.eulerAngles;
		baseEuler.x = ballTrans.eulerAngles.x;

		ballTrans.rotation = Quaternion.Euler(baseEuler);

		///////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////

		//THis disables the movement if the player has been bumped
		if (bumped)
		{
			transform.position = Vector3.MoveTowards(transform.position,
				transform.position + (bumpDirection * 2f),
				speed * Time.deltaTime);

			if(bumpTimer > 0f)
			{
				bumpTimer -= Time.deltaTime;
			}
			else
			{
				bumped = false;
				bumpDirection = Vector3.zero;
				bumpTimer = 0f;
				baseBall.CmdSetBumped(false);
			}
		}
		else
		{
			//This is the normal movement line
			transform.position = Vector3.MoveTowards(transform.position,
				transform.position + (transform.forward * (reverse ? -2f : 2f)),
				currentSpeed * Time.deltaTime);

		}

		//Only draw the trails if we're moving
		if (currentSpeed > 0f)
		{
			engineAudio.volume = Mathf.Lerp(0f, 0.3f, moveRate);
			engineAudio.pitch = Mathf.Lerp(0.75f, 1.1f, moveRate);



			currentSpeed -= 0.1f;


			if (soundDelay > 0f)
				soundDelay -= Time.deltaTime;

			if (trailTime > Mathf.Lerp(0.1f, 0.01f, moveRate))
			{
				if (LocalPlayerGameController.Instance.SpawnTrail(transform))
				{
					if (playFallImpact && soundDelay <= 0f)
					{
						soundDelay = 0.12f;
						playFallImpact = false;
						Sounds.SoundManager.PlayEffect(Sounds.SOUND.FALL, false);
						partsSystemDirt.Emit(10);

						if (isLocalPlayer && cam != null)
							cam.ShakeCamera(0.2f, 0.35f * moveRate);
					}
					else
					{
						playFallImpact = false;
					}
				}
				else
					playFallImpact = true;


				trailTime = 0f;
			}
			else
				trailTime += Time.deltaTime;

			partsSystemDirt.emissionRate = Mathf.Lerp(0f, 10f, moveRate);

			if (!partsSystemDirt.isPlaying)
				partsSystemDirt.Play();
		}
		else
			reverse = false;
		

		rot = 0f;

	}
#endregion

#region UI Button Presses
	public void OnGasPressed()
	{
		if (reverse && currentSpeed > 0f)
		{
			currentSpeed -= 0.5f;
			return;
		}
		reverse = false;

		if (currentSpeed < realSpeed)
			currentSpeed += 0.5f;

		rot += 5;
	}

	public void OnReversePressed()
	{
		if(!reverse && currentSpeed > 0f)
		{
			currentSpeed -= 0.5f;
			return;
		}
		reverse = true;

		if (currentSpeed < realSpeed)
			currentSpeed += 0.5f;
		rot -= 5;
	}

	public void OnUsePressed()
	{
        powerUp.GetComponent<OnUseButtonBehaviour>().OnUse();
	}

#endregion //UI Button Presses

	public void BumpMe(Vector3 direction, float time)
	{
		//Checking if already bumped
		if (bumped)
			return;

		if (isLocalPlayer && cam != null)
			cam.ShakeCamera(0.5f, 1f);

        baseBall.CmdSetBumped(true);
		bumped = true;
		bumpDirection = (direction - transform.position);
		bumpTimer = time;
	}


    public void NewPowerUp(int id)
    {
        OnUseButton.GetComponent<OnUseButtonBehaviour>().NewPowerUp(id);

    }
}
