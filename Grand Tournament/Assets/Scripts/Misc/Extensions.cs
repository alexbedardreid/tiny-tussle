﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Vector3Extensions
{
	public static Vector3 Clamp(this Vector3 value, Vector3 min, Vector3 max)
	{
		value.x = Mathf.Clamp(value.x, min.x, max.x);
		value.y = Mathf.Clamp(value.y, min.y, max.y);
		value.z = Mathf.Clamp(value.z, min.z, max.z);


		return value;
	}
}

public static class EnumerableExtension
{
	public static T PickRandom<T>(this IEnumerable<T> source)
	{
		return source.PickRandom(1).Single();
	}

	public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
	{
		return source.Shuffle().Take(count);
	}

	public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
	{
		return source.OrderBy(x => Guid.NewGuid());
	}
}