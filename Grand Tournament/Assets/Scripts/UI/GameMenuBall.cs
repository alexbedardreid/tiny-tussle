﻿using UnityEngine;
using System.Collections;

public class GameMenuBall : MonoBehaviour
{
	public BallGroup frontBall;
	public BallGroup backBall;

	public MaterialGroup Hamsters;
	public MaterialGroup Gerbils;

	public static MaterialGroup _Hamsters;
	public static MaterialGroup _Gerbils;

	#region Script Instance

	private static GameMenuBall instance = null;

	protected GameMenuBall() { }

	public static GameMenuBall Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		if (GameMenuBall.Instance && !(GameMenuBall.Instance == this))
			this.gameObject.SetActive(false);
		else
			instance = this;
	}
	#endregion

	// Use this for initialization
	void Start()
	{
		//Set Global Material Values
		_Hamsters = Hamsters;
		_Gerbils = Gerbils;


        frontBall.BallRenderer.material = Hamsters.BallMaterial;
		frontBall.RodentRenderer.material = Hamsters.RodentMaterial;

		backBall.BallRenderer.material = Gerbils.BallMaterial;
		backBall.RodentRenderer.material = Gerbils.RodentMaterial;
	}

	public void SetTo(bool isHamster)
	{
		if(isHamster)
		{
			frontBall.BallRenderer.material = Hamsters.BallMaterial;
			frontBall.RodentRenderer.material = Hamsters.RodentMaterial;

			backBall.BallRenderer.material = Gerbils.BallMaterial;
			backBall.RodentRenderer.material = Gerbils.RodentMaterial;
		}
		else
		{
			backBall.BallRenderer.material = Hamsters.BallMaterial;
			backBall.RodentRenderer.material = Hamsters.RodentMaterial;

			frontBall.BallRenderer.material = Gerbils.BallMaterial;
			frontBall.RodentRenderer.material = Gerbils.RodentMaterial;
		}
    }
}

[System.Serializable]
public struct BallGroup
{
	public Renderer BallRenderer;
	public Renderer RodentRenderer;
}

[System.Serializable]
public struct MaterialGroup
{
	public Material BallMaterial;
	public Material RodentMaterial;
}
