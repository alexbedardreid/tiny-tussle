﻿using UnityEngine;
using System;
using System.Collections;

public class ThirdPersonCam : MonoBehaviour {

    
	[SerializeField]
	public Transform playerTrans;
	[SerializeField]
	public Transform playerCameraPointTrans;
	[SerializeField]
	Transform camPivotTransform;
	[SerializeField]
	Transform camTransform;

	CameraShake camShake;

	new Transform transform;

    Vector3 offSet;
    public float damping = 1;


	float currentAngle;
	float desiredAngle;
	//float angleLerp;


	public Vector3 SMOOTH_TIME = new Vector3(0.2f, 0.5f, 0.2f);
	Vector3 CameraLockPos;
	Vector3 velocity;
	Vector3 lookVelocity;

	// Use this for initialization
	public void Init () 
	{

		transform = this.gameObject.transform;
		transform.localPosition = new Vector3(0f, 1.3f, -4.2f);

		camShake = this.GetComponent<CameraShake>();
		//offSet = playerTrans.position - camTransform.position;

	}

	public void ShakeCamera()
	{
		ShakeCamera(1f);
    }
	public void ShakeCamera(float length, float intensity = 0.7f)
	{
		camShake.shakeAmount = intensity;

		camShake.shake = length;
    }

    void LateUpdate()
    {
		if (!playerTrans)
			return;

		//transform.position = playerTrans.position;

		//currentAngle = transform.eulerAngles.y;
		//desiredAngle = playerTrans.eulerAngles.y;
		//
		////angleLerp = Mathf.LerpAngle(currentAngle, desiredAngle, Time.deltaTime * damping);
		//
		//Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);

		//camTransform.position = playerTrans.position - (rotation * offSet);
		//transform.position = new Vector3(0f, 1.12f, -4.37f);
		//camTransform.LookAt(playerTrans);

		SmoothCam();
		SmoothCamLook();
    }


	Vector3 newPos = Vector3.zero;
	void SmoothCam()
	{
		newPos = Vector3.zero;

		newPos.x = Mathf.SmoothDamp(camPivotTransform.position.x, playerCameraPointTrans.position.x, ref velocity.x, SMOOTH_TIME.x);
		newPos.y = Mathf.SmoothDamp(camPivotTransform.position.y, playerCameraPointTrans.position.y, ref velocity.y, SMOOTH_TIME.y);
		newPos.z = Mathf.SmoothDamp(camPivotTransform.position.z, playerCameraPointTrans.position.z, ref velocity.z, SMOOTH_TIME.z);

		camPivotTransform.position = newPos;
	}

	Vector3 newPosLook = Vector3.zero;
	void SmoothCamLook()
	{
		newPosLook = Vector3.zero;

		newPosLook.x = Mathf.SmoothDamp(camPivotTransform.forward.x, playerTrans.forward.x, ref lookVelocity.x, SMOOTH_TIME.x);
		newPosLook.y = Mathf.SmoothDamp(camPivotTransform.forward.y, playerTrans.forward.y, ref lookVelocity.y, SMOOTH_TIME.y);
		newPosLook.z = Mathf.SmoothDamp(camPivotTransform.forward.z, playerTrans.forward.z, ref lookVelocity.z, SMOOTH_TIME.z);

		camPivotTransform.forward = newPosLook;
	}
}

