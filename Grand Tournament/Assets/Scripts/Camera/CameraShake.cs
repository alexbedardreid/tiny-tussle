﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraShake : MonoBehaviour
{
	new public Camera camera;
	public float shake = 0f;
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1f;

	Transform camTrans;
	Vector3 startPos;
	// Use this for initialization
	void Start()
	{
		if (!camera)
		{
			camera = this.GetComponent<Camera>();
			camTrans = camera.transform;
		}
		else
			camTrans = camera.transform;

		startPos = camTrans.localPosition;
    }

	// Update is called once per frame
	void Update()
	{
		if(shake > 0)
		{
			camTrans.localPosition = Vector3.Lerp(startPos, startPos + (Random.insideUnitSphere * shakeAmount), shake);
			//camTrans.localPosition = Random.insideUnitSphere * shakeAmount;
			shake -= Time.deltaTime * decreaseFactor * decreaseFactor;
		}
		else
		{
			shake = 0;
		}
	}

#if UNITY_EDITOR

	[Space(20)]
	public float TestShake = 1f;

	[ContextMenu("Shake Shake Shake...")]
	void ShakeMe()
	{
		if (!camera)
		{
			camera = this.GetComponent<Camera>();
			camTrans = camera.transform;
		}

		if(camTrans == null)
		{
			camTrans = camera.transform;
		}

		shake = TestShake;
    }

#endif

}
