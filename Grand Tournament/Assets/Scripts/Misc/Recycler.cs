﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Recycling
{
	public class Recycler : MonoBehaviour
	{
		static Dictionary<Type, RecycleBin> SortingDict = new Dictionary<Type, RecycleBin>();
		static RecycleBin bin;

		new static Transform transform;

		#region Script Instance

		private static Recycler instance = null;

		protected Recycler() { }

		void Awake()
		{
			instance = this;
			transform = gameObject.transform;
		}
		#endregion

		void OnDestroy()
		{
			SortingDict.Clear();
			transform = null;
		}


		public static void Recycle(Type _obj, GameObject _gameObject)
		{

			if (!SortingDict.TryGetValue(_obj, out bin))
			{
				bin = new RecycleBin();
				SortingDict.Add(_obj, bin);

			}

			bin.Store(_gameObject);
			_gameObject.transform.parent = transform;
		}

		public static bool TryGrab(Type _type, out GameObject _gameObject, bool returnActive = true)
		{
			_gameObject = null;

			if (SortingDict.TryGetValue(_type, out bin))
			{
				if (bin.Grab(out _gameObject))
				{
					if (returnActive) _gameObject.SetActive(true);
					return true;
				}

			}
			return false;
		}
	}

	class RecycleBin
	{
		Stack<GameObject> recycled;

		public void Store(GameObject _gobj)
		{
			if (this.recycled == null) this.recycled = new Stack<GameObject>();

			_gobj.SetActive(false);
			this.recycled.Push(_gobj);

		}

		public bool Grab(out GameObject _go)
		{
			if (this.recycled == null || this.recycled.Count <= 0)
			{
				_go = null;
				return false;
			}
			_go = recycled.Pop();
			return true;
		}


	}
}
